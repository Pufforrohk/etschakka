
package unitn.akkaetsch;

import unitn.akkaetsch.structures.Algorithm;
import unitn.akkaetsch.structures.Graph;
import java.util.ArrayList;

public class ConnectedComponents extends Algorithm{

    @Override
    public void Init(Graph g) {
        for(int i:g.nodes){
            g.setState(i, i);
        }
    }

    @Override
    public void Local(Graph g) {
        System.out.println("EXECUTING LOCAL");
        for(int i:g.nodes){
            System.out.println(i + "= " + g.getState(i));
        }
        for(int i:g.nodes){
            int nei;
            while((nei=g.nextNeighbor(i))!=-1){
                if((Integer)g.getState(nei) > (Integer) g.getState(i))
                    g.setState(nei, (Integer) g.getState(i) + 0);
            }
        }
    }

    @Override
    public Integer Aggregation(Object ol, ArrayList<Object> newstates) {
        Integer old = (Integer) ol;
        for(Object state:newstates)
            if((Integer) state<old)
                old = (Integer) state;
        return old;
    }
    
}
