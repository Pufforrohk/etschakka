package unitn.akkaetsch;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import java.io.File;

public class Configuration {

    private static Configuration config = null;
    public final Config conf;

    public Configuration(String filename) {
        conf = ConfigFactory.parseFile(new File(filename));
    }

    public static Configuration getConfig() {
        if (config == null) {
            config = new Configuration("application.conf");
        }
        return config;
    }

    public String getMasterPath(){
        return conf.getList("akka.cluster.seed-nodes").get(0).unwrapped() + "/user/master";
    }
    
    public int getSlaves() {
        return conf.getInt("unitn.slaves");
    }

    public boolean autoStop() {
        if(conf.hasPath("unitn.autostop")){
            return conf.getBoolean("unitn.autostop");        
        }else{
            return true;
        }
    }
    
    public int maxIterations(){
        if(conf.hasPath("unitn.max-iterations")){
            return conf.getInt("unitn.max-iterations");        
        }else{
            return Integer.MAX_VALUE;
        }
        
    }

}
