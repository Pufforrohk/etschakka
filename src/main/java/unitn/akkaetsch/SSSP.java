package unitn.akkaetsch;

import it.unimi.dsi.fastutil.AbstractIndirectPriorityQueue;
import it.unimi.dsi.fastutil.objects.ObjectArrayIndirectPriorityQueue;
import it.unimi.dsi.fastutil.objects.ObjectHeapIndirectPriorityQueue;
import unitn.akkaetsch.structures.Algorithm;
import unitn.akkaetsch.structures.Graph;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class SSSP extends Algorithm {

    public static int SOURCE = 1263743;
    public static final int MAXVALUE = Integer.MAX_VALUE - 10;

    @Override
    public void Init(Graph g) {
        System.out.println("MY SOURCE IS " + SOURCE);
        for (int id : g.nodes) {
            if (id == SOURCE) {
                System.out.println("INITED SOURCE");
                g.setState(id, 0);
            } else {
                g.setState(id, MAXVALUE);
            }
        }
    }

    private class pair implements Comparable<pair> {

        final int id;
        final int dist;

        public pair(int i, int d) {
            id = i;
            dist = d;
        }

        @Override
        public int compareTo(pair o) {
            return dist - o.dist;
        }

    }

    private class statecomp implements Comparator<Object> {

        @Override
        public int compare(Object o1, Object o2) {
            return (Integer) o1 - (Integer) o2;
        }
    }

    
    
@Override
    public void Local(Graph g) {

        AbstractIndirectPriorityQueue<Object> pq = new ObjectHeapIndirectPriorityQueue<>(g.state);//new ObjectHeapIndirectPriorityQueue<>(g.state, new statecomp());

        int s = 0;
        for (int i = 0; i < g.edges.length; i++) {
            while (i == g.st[s + 1]) {
                s++;
            }
            int el = g.edges[i];
            int dist = (Integer) g.state[s];
            if (dist != MAXVALUE && dist + 1 < (Integer) g.state[el]) {
                pq.enqueue(s);
                i = g.st[s + 1] - 1;
                s++;
            }
        }
        System.out.println("SIZE IS " + pq.size());
        while (!pq.isEmpty()) {
            int id = pq.dequeue();
            int dist = (Integer) g.state[id];
            if (dist != MAXVALUE) {
                //message+= " YES ";
                for (int i = g.st[id]; i < g.st[id + 1]; i++) {
                    int nei = g.edges[i];

                    if ((Integer) g.state[nei] > dist + 1) {
                        g.state[nei] = dist + 1;
                        g.addChanged(nei);
                        if (!pq.contains(nei)) {
                            pq.enqueue(nei);
                        } else {
                            pq.changed(nei);
                        }
                    }
                }
            }
        }
    }

    @Override
    public Object Aggregation(Object old, ArrayList<Object> newstates) {
        Integer dist = (Integer) old;
        if (dist == null) {
            dist = MAXVALUE;
        }
        for (Object obj : newstates) {
            if (obj != null) {
                if ((Integer) obj < dist) {
                    dist = (Integer) obj;
                }
            }
        }
        return dist;
    }

}
