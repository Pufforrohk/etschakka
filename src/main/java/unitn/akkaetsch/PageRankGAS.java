package unitn.akkaetsch;

import unitn.akkaetsch.structures.GAS;
import unitn.akkaetsch.structures.Graph;
import java.io.Serializable;
import java.util.ArrayList;

public class PageRankGAS extends GAS implements Serializable {

    public static final double STARTING_VALUE = 1.0;

    public class State implements Serializable {

        double v;
        int deg;

        public State(double vv, int d) {
            deg = d;
            v = vv;
        }
        
        @Override
        public String toString(){
            return "(" + deg + "," + v + ")";
        }
       
    }

    @Override
    public void Local(Graph g) {
        if (iteration == 1) {
            g.resetNode();
            int n;
            while((n=g.nextNode())!=-1){
                State s = new State(STARTING_VALUE,g.getDegree(n));
                g.setCurrentNode(s);
            }
        } else {
            super.Local(g);
        }
    }

    @Override
    public Object Aggregation(Object old, ArrayList<Object> newstates) {
        if (iteration == 1) {
            State s = new State(STARTING_VALUE, 0);
            for (Object st : newstates) {
                s.deg += ((State) st).deg;
            }
            return s;
        } else {
            return super.Aggregation(old, newstates);
        }
    }

    @Override
    public Object Gather(Object message1, Object message2) {
        return (Double) message1  + (Double) message2;
    }

    @Override
    public Object Apply(Object state, Object message) {
        State s = (State) state;
        s.v = 0.15 + 0.85 * (Double) message;
        return s;
    }

    @Override
    public Object Scatter(Object state, int nodeDest) {
        State s = (State)state;
        return s.v/s.deg;
    }

    @Override
    public Object Init(int nodeId) {
        return 0;
    }

}
