package unitn.akkaetsch.structures;

import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashBigSet;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Map;

public class Graph {

    public int[] changed;
    public int numchanged;

    public int[] owns;
//    public HashSet<Integer> changed;
    public Int2IntMap owner;
    public int[] nodes;
    //public boolean[] changed;

    public int[] edges;
    public int[] st;
    public int[] id;
    public Object[] state;
    protected final Int2IntMap mp;
//    private HashMap<Integer, Integer> mp;

    public void fillOwners() {
        owns = new int[id.length];
        Arrays.fill(owns, -1);
        for (Int2IntMap.Entry en : owner.int2IntEntrySet()) {
            owns[mp.get(en.getIntKey())]=en.getIntValue();
        }
        owner=null;
    }

    public int getDegree(int id) {
        if (!mp.containsKey(id)) {
            return 0;
        }
        int p = mp.get(id);
        return st[p + 1] - st[p];
    }

    public void resetChanged() {
        numchanged = 0;
    }

    public void addChanged(int i) {
        changed[numchanged] = i;
        numchanged++;
    }

    protected int nextNodeCur = 0;

    public void resetNode() {
        nextNodeCur = 0;
    }

    public int nextNode() {
        if (nextNodeCur == id.length) {
            return -1;
        }
        int v = id[nextNodeCur];
        nextNodeCur++;
        return v;
    }

    public void setCurrentNode(Object s) {
        addChanged(nextNodeCur - 1);
//        changed[nextNodeCur-1]=true;
        state[nextNodeCur - 1] = s;
    }

    protected int nextNeighborCur = -1;
    protected int nextNeighborPos;
    protected int nextNeighborEdge = 0;

    public int nextNeighbor(int node_id) {
        if (nextNeighborCur != node_id) {
            nextNeighborCur = node_id;
            if (!mp.containsKey(node_id)) {
                return -1;
            }
            nextNeighborPos = mp.get(node_id);
            nextNeighborEdge = st[nextNeighborPos];
        }
        if (nextNeighborEdge < st[nextNeighborPos + 1]) {
            nextNeighborEdge++;
            return id[edges[nextNeighborEdge - 1]];
        } else {
            return -1;
        }
    }

    public Graph() {
        owner = new Int2IntOpenHashMap();
        mp = new Int2IntOpenHashMap();
//        changed = new IntOpenHashBigSet();
    }

    public Object getState(int id) {
        return state[mp.get(id)];
    }

    public void setState(int id, Object newstate) {
        if (newstate == null) {
            new Exception().printStackTrace();
            System.exit(1);
        }
        int p = mp.get(id);
        addChanged(p);
        state[p] = newstate;
    }

    public static Graph loadEdgeList(String filename) {

        Graph gr = new Graph();
        try {

            String l;
            int counter = 0;
            Int2IntMap deg = new Int2IntOpenHashMap();

            BufferedReader br = new BufferedReader(new FileReader(filename));
            while ((l = br.readLine()) != null) {
                int in = l.indexOf(' ');
                if (counter % 1000000 == 0) {
                    System.out.println(counter);
                }

                int id = Integer.parseInt(l.substring(0, in));
                int id2 = Integer.parseInt(l.substring(in + 1, l.length()));
                if (!deg.containsKey(id2)) {
                    deg.put(id2, 0);
                }

                if (!deg.containsKey(id)) {
                    deg.put(id, 0);
                }
                deg.put(id, deg.get(id) + 1);

                counter++;
            }
            System.out.println("Loaded first");
            gr.st = new int[deg.size() + 1];
            gr.edges = new int[counter];
            gr.id = new int[deg.size()];
            gr.changed = new int[deg.size()];
            System.out.println("Allocated arrays");
            int cur = 0;
            int i = 0;
            gr.mp.putAll(deg);
            for (Int2IntOpenHashMap.Entry en : deg.int2IntEntrySet()) {
                int id = en.getKey();
                gr.mp.put(id, i);
                gr.st[i] = cur;
                gr.id[i] = id;
                cur += en.getValue();
                i++;
                if (i % 100000 == 0) {
                    System.out.println(i + " " + deg.size());
                }
            }
            gr.st[deg.size()] = cur;
            System.out.println("Filled arrays");
            br = new BufferedReader(new FileReader(filename));
            counter = 0;
            while ((l = br.readLine()) != null) {
                int in = l.indexOf(' ');
                if (counter % 1000000 == 0) {
                    System.out.println(counter);
                }
//                System.out.println(l + ":" + in + ":" + line0 + ":" + line1);
                int id = Integer.parseInt(l.substring(0, in));
                int id2 = Integer.parseInt(l.substring(in + 1, l.length()));

                int d = deg.get(id) - 1;
                deg.put(id, d);
                gr.edges[gr.st[gr.mp.get(id)] + d] = gr.mp.get(id2);
                counter++;
            }
            {
                gr.nodes = new int[deg.size()];
//                gr.changed = new boolean[deg.size()];
                gr.state = new Object[deg.size()];
                int ii = 0;
                for (int el : deg.keySet()) {
                    gr.nodes[ii] = el;
                    ii++;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        return gr;
    }
}
