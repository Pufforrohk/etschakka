package unitn.akkaetsch.structures;

import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import java.util.ArrayList;

public abstract class GAS extends Algorithm {

    public abstract Object Gather(Object message1, Object message2);

    public abstract Object Apply(Object state, Object message);

    public abstract Object Scatter(Object state, int nodeDest);

    public abstract Object Init(int nodeId);

    @Override
    public void Init(Graph g) {
        for (int i = 0; i < g.id.length; i++) {
            int node = g.id[i];
            g.state[i] = Init(node);
        }
    }

    @Override
    public void Local(Graph g) {
        Object[] local_states = new Object[g.nodes.length];
        int node = 0;
        Object st = null;
        System.out.println("START");
        for (int i = 0; i < g.edges.length; i++) {
            int dest = g.edges[i];
            if (i == g.st[node]) {
                while(i==g.st[node+1])
                    node++;
                st = g.state[node];
                node++;
            }
            Object temp = Scatter(st, g.id[dest]);
            if (temp != null) {
                if (local_states[dest] == null) {
                    local_states[dest] = temp;
                } else {
                    local_states[dest] = Gather(local_states[dest], temp);
                }
            }
        }
        System.out.println("HERE");
        
        for (int i = 0; i < local_states.length; i++) {
            if (local_states[i] != null) {
                if (g.owns[i]==-1) {
                    g.state[i] = Apply(g.state[i], local_states[i]);
                } else {
                    g.state[i] = local_states[i];
                }
                g.addChanged(i);
            }
        }
    }

    @Override
    public Object Aggregation(Object old, ArrayList<Object> newstates) {
        Object st = null;
        for (Object state : newstates) {
            if (st == null) {
                st = state;
            } else {
                st = Gather(st, state);
            }
        }
        if (st == null) {
            return old;
        }
        if (old == null) {
            return st;
        }
        return Apply(old, st);
    }

}
