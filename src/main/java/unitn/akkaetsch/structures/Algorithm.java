package unitn.akkaetsch.structures;

import unitn.akkaetsch.structures.Graph;
import java.util.ArrayList;

public abstract class Algorithm {
    public int iteration;
    
    public abstract void Init(Graph g);
    public abstract void Local(Graph g);
    public abstract Object Aggregation(Object old, ArrayList<Object> newstates);
}
