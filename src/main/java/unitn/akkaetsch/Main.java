package unitn.akkaetsch;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import java.util.Scanner;
import unitn.akkaetsch.syncronous.SynMaster;
import unitn.akkaetsch.syncronous.SynMessages.CHECKIN;
import unitn.akkaetsch.syncronous.SynPartition;

public class Main {

    public static String ip;

    public static void master() {
        Configuration conf = Configuration.getConfig();
        
        Config config = ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + ip)
                .withFallback(conf.conf);
        Integer slaves = conf.getSlaves();
        ActorSystem system = ActorSystem.create("Etsch", config);
        ActorRef master = system.actorOf(SynMaster.props(slaves), "master");
    }

    public static void node(int id, String inputfile, boolean GAS) {
        Configuration conf = Configuration.getConfig();

        int port = 0; //random port
        Config config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port)
                .withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + ip))
                .withFallback(conf.conf);

        String masterPath = conf.getMasterPath();

        ActorSystem system = ActorSystem.create("Etsch", config);
        Class cl = SSSP.class;
        if (GAS) {
            cl = PageRankGAS.class;
        }
        ActorRef node = system.actorOf(SynPartition.props(id, inputfile, cl, masterPath), "node" + id);
        node.tell(new CHECKIN(id), null); //Tell the node actor to talk to the master node
    }

    public static void main(String[] argv) throws InterruptedException {
        if (argv[0].equals("ec2")) {
            ip = getIP();
        } else if (argv[0].equals("local")) {
            ip = "127.0.0.1";
        }

        if (argv[1].equals("master")) {
            master();
        } else if (argv[1].equals("all")) {
            master();
            Thread.sleep(1000);
            SSSP.SOURCE = Integer.parseInt(argv[2]);
            boolean GAS = Boolean.parseBoolean(argv[3]);
            for (int i = 0; i < 2; i++) {
                node(i, "p" + i, GAS);
            }
        } else {
            int id = Integer.parseInt(argv[1]);
            String path = argv[2];
            SSSP.SOURCE = Integer.parseInt(argv[3]);
            boolean GAS = Boolean.parseBoolean(argv[4]);
            node(id, path, GAS);
        }

    }

    //Gets local ip via semi-ugly curl call
    public static String getIP() {
        try {
            Process p = Runtime.getRuntime().exec("curl http://169.254.169.254/latest/meta-data/local-ipv4");
            int returnCode = p.waitFor();
            if (returnCode == 0) {
                Scanner s = new Scanner(p.getInputStream());
                String ip = s.nextLine();
                s.close();
                return ip;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
            return null;
        }
    }
}
