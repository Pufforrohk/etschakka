package unitn.akkaetsch;

import unitn.akkaetsch.structures.GAS;

public class SSSP_GAS extends GAS{

    public static final int MAXINT = Integer.MAX_VALUE-10;
    
    @Override
    public Object Gather(Object message1, Object message2) {
        return Math.min((Integer) message1, (Integer)message2);
    }

    @Override
    public Object Apply(Object state, Object message) {
        return Math.min((Integer) state, (Integer)message);
    }

    @Override
    public Object Scatter(Object state, int nodeDest) {
        Integer st = (Integer) state;
        if(st!=MAXINT)
            return st+1;
        else
            return null;
    }


    @Override
    public Object Init(int nodeId) {
        if(nodeId==SSSP.SOURCE)
            return 0;
        else
            return MAXINT;
    }
    
}
