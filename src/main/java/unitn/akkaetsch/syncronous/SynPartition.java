package unitn.akkaetsch.syncronous;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.japi.Creator;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import unitn.akkaetsch.Configuration;
import unitn.akkaetsch.structures.Algorithm;
import unitn.akkaetsch.structures.Graph;
import unitn.akkaetsch.syncronous.SynMessages.Pair;

public class SynPartition extends UntypedActor {

    public int ID;

    Graph subgraph;
    Int2ObjectOpenHashMap<SynReplica> myreplicas;
    Algorithm alg;
    Class algC;
    ActorRef master;
    final String filename;
    final String masterPath;
    ArrayList<ActorRef> partitions;

    public static Props props(final int id, final String filename, final Class c, final String masterPath) {
        return Props.create(new Creator<SynPartition>() {
            @Override
            public SynPartition create() throws Exception {
                return new SynPartition(id, filename, c, masterPath);
            }
        });
    }

    public SynPartition(int id, String filename, Class algo, String masterPath) {
        this.ID = id;
        algC = algo;
        myreplicas = new Int2ObjectOpenHashMap<>();
        replicas_helper = new HashMap<>();
        this.masterPath = masterPath;
        this.filename = filename;
        Configuration c = Configuration.getConfig();
        maxIterations = c.maxIterations();
        autostop = c.autoStop();
        try {
            alg = (Algorithm) algo.newInstance();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    boolean nextlocal = true;
    int receivedReplicas1 = 0;
    int receivedReplicas2 = 0;
    int receivedLocal = 0;
    int receivedAggr = 0;
    int numberupdates = 0;
    long timeinLocal = 0;
    long timeinAggr = 0;
    final int maxIterations;
    final boolean autostop;
    int iteration = 0;
    long startingTime;

    public void doInit() {
        alg.Init(subgraph);
        for (SynReplica rep : myreplicas.values()) {
            rep.setState(subgraph.getState(rep.id));
        }
        subgraph.resetChanged();
    }

    public void doLocal() {
        iteration++;
        alg.iteration = iteration;
        System.out.println(ID + " iteration " + iteration + " " + numberupdates);

        if (iteration > maxIterations || (numberupdates == 0 && autostop == true)) {
            master.tell(new SynMessages.NOTHINGTODO(ID, iteration), self());
        }
        if (iteration > maxIterations) {
            return;
        }
        numberupdates = 0;
        subgraph.resetChanged();
        long currentTime = System.currentTimeMillis();

        alg.Local(subgraph);

        long endtime2 = System.currentTimeMillis();
        System.out.println("LOCAL1 = " + (endtime2 - currentTime));

        ArrayList<ArrayList<Pair>> messages = new ArrayList<>();
        for (int i = 0; i < partitions.size(); i++) {
            messages.add(new ArrayList<Pair>());
        }
        
        for(int j=0;j<subgraph.numchanged;j++){
            int i = subgraph.changed[j];
            int node = subgraph.id[i];
//        for (int i = 0; i < subgraph.changed.length; i++) {
//            if (subgraph.changed[i]) {
//                int node = subgraph.id[i];
                if (subgraph.owns[i]!=-1) {
                    int owner = subgraph.owns[i];
                    if (owner == ID) {
                        SynReplica rep = myreplicas.get(node);
                        rep.addState(subgraph.getState(node));
                    } else {
                        messages.get(owner).add(new Pair(node, subgraph.getState(node)));
                    }
                } else {
                    numberupdates++;
                }
//            }
        }
        subgraph.resetChanged();
        for (int i = 0; i < partitions.size(); i++) {
            if (i != ID) {
                partitions.get(i).tell(new SynMessages.UPDATES_FROM_LOCAL(ID, messages.get(i)), self());
            }
        }

        long endtime = System.currentTimeMillis();
        System.out.println("LOCAL = " + (endtime - currentTime));
        double sec = (endtime - startingTime) / 1000.0;
        System.out.println("TOTAL = " + sec + " (" + (sec / iteration) + ")");
        timeinLocal += (endtime - currentTime);
    }

    public void doAggr() {
        long currentTime = System.currentTimeMillis();
        ArrayList<ArrayList<Pair>> messages = new ArrayList<>();
        for (int i = 0; i < partitions.size(); i++) {
            messages.add(new ArrayList<Pair>());
        }
        //       String str = "AGGREGATION FOR PARTITION " + ID + "\n";
        for (int node : myreplicas.keySet()) {
            SynReplica rep = myreplicas.get(node);
            //           str += node + "=" + rep.obj + "\n";
            if (rep.getState() == null) {
                System.out.println("FAIL!!!" + rep.id);
            }
            boolean sendback = rep.update(alg);

            //           str += node + "=" + rep.obj + "\n";
            if (sendback) {
                for (int part : rep.partitions) {
                    messages.get(part).add(new Pair(node, rep.getState()));
                }
            }
            if (rep.changed) {
                //               str += "CHANGED\n";
                numberupdates++;
            }
            if (rep.getState() == null) {
                System.out.println("FAIL!!!" + rep.id);
            }
            subgraph.setState(node, rep.getState());
        }
        //       System.out.println(str);
        for (int i = 0; i < partitions.size(); i++) {
            if (i != ID) {
                partitions.get(i).tell(new SynMessages.UPDATES_FROM_AGGR(ID, messages.get(i)), self());
            }
        }
        long endtime = System.currentTimeMillis();
        System.out.println("AGGR = " + (endtime - currentTime));
        timeinAggr += (endtime - currentTime);
    }

    public void print() {
        String line = "";
        for (int id : subgraph.nodes) {
            line += ID + "\t" + iteration + " " + id + " = " + subgraph.getState(id) + "\n";
        }
        System.out.println(line + "\n");
    }
    HashMap<Integer, ArrayList<Integer>> replicas_helper;

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof SynMessages.CHECKIN) {
            long t = System.currentTimeMillis();
            subgraph = Graph.loadEdgeList(filename);
            System.out.println("LOADED IN " + (System.currentTimeMillis() - t));
            getContext().actorSelection(masterPath).tell(new SynMessages.CHECKIN(ID), self());
        } else if (o instanceof SynMessages.SENDREPLICAS) {
            System.out.println("PREPARING REPLICAS");
            SynMessages.SENDREPLICAS mess = (SynMessages.SENDREPLICAS) o;
            partitions = mess.partitions;
            master = getSender();
            ArrayList<ArrayList<Integer>> replicas = new ArrayList<>();
            for (int i = 0; i < partitions.size(); i++) {
                replicas.add(new ArrayList<Integer>());
            }
            for (int node : subgraph.nodes) {
                replicas.get(node % partitions.size()).add(node);
            }
            for (int i = 0; i < partitions.size(); i++) {
                partitions.get(i).tell(new SynMessages.MYREPLICAS(replicas.get(i), ID), self());
            }
            System.out.println("SENT REPLICAS");
        } else if (o instanceof SynMessages.MYREPLICAS) {
            receivedReplicas1++;
            SynMessages.MYREPLICAS mess = (SynMessages.MYREPLICAS) o;
            System.out.println("RECEIVED REPLICAS FROM " + mess.partition_id);
            for (int node : mess.nodes) {
                if (!replicas_helper.containsKey(node)) {
                    replicas_helper.put(node, new ArrayList<Integer>());
                }
                replicas_helper.get(node).add(mess.partition_id);
            }
            if (receivedReplicas1 == partitions.size()) {
                ArrayList<HashMap<Integer, Integer>> or = new ArrayList<>();
                ArrayList<HashMap<Integer, ArrayList<Integer>>> yr = new ArrayList<>();
                for (int i = 0; i < partitions.size(); i++) {
                    or.add(new HashMap<Integer, Integer>());
                    yr.add(new HashMap<Integer, ArrayList<Integer>>());
                }
                Random r = new Random();
                for (int node : replicas_helper.keySet()) {
                    ArrayList<Integer> part = replicas_helper.get(node);
                    if (part.size() > 1) {
                        int owner = part.get(r.nextInt(part.size()));
                        yr.get(owner).put(node, new ArrayList<Integer>());
                        for (int p : part) {
                            if (p != owner) {
                                yr.get(owner).get(node).add(p);
                                or.get(p).put(node, owner);
                            }
                        }
                    }
                }
                for (int i = 0; i < partitions.size(); i++) {
                    partitions.get(i).tell(new SynMessages.ANSWER_REPLICAS(or.get(i), yr.get(i)), self());
                }
                System.out.println("PROCESSED ALL REPLICAS");
                replicas_helper.clear();
            }
        } else if (o instanceof SynMessages.ANSWER_REPLICAS) {
            receivedReplicas2++;
            SynMessages.ANSWER_REPLICAS mess = (SynMessages.ANSWER_REPLICAS) o;
            System.out.println("GOT REPLICAS");
            for (int node : mess.other_replicas.keySet()) {
                int val = mess.other_replicas.get(node);
                if (val != -1) {
                    subgraph.owner.put(node, val);
                }
            }
            for (int node : mess.your_replicas.keySet()) {
                SynReplica replica = new SynReplica(node);
                for (int part : mess.your_replicas.get(node)) {
                    replica.partitions.add(part);
                }
                subgraph.owner.put(node, ID);
                myreplicas.put(node, replica);
            }
            if (receivedReplicas2 == partitions.size()) {
                System.out.println("DONE WITH REPLICAS " + myreplicas.size());
                subgraph.fillOwners();
                System.out.println("FILLED OWNERS " + myreplicas.size());
                master.tell(new SynMessages.DONE_REPLICAS(), self());
                System.gc();
            }
        } else if (o instanceof SynMessages.START_ETSCH) {
            startingTime = System.currentTimeMillis();
            doInit();
            numberupdates = 1;
            doLocal();
            nextlocal = false;
        } else if (o instanceof SynMessages.UPDATES_FROM_AGGR) {
            receivedAggr++;
            SynMessages.UPDATES_FROM_AGGR up = (SynMessages.UPDATES_FROM_AGGR) o;
//            System.out.println(ID + " received " + up.updates.size() + " from " + up.partition_id);
            for (Pair p : up.updates) {
                subgraph.setState(p.id, p.state);
            }

            if (receivedAggr == partitions.size() - 1 && nextlocal) {
                doLocal();
                nextlocal = false;
                receivedAggr = 0;
                if (receivedLocal == partitions.size() - 1) {
                    doAggr();
                    receivedLocal = 0;
                    nextlocal = true;
                }
            }
        } else if (o instanceof SynMessages.UPDATES_FROM_LOCAL) {

            receivedLocal++;

            SynMessages.UPDATES_FROM_LOCAL up = (SynMessages.UPDATES_FROM_LOCAL) o;

            for (Pair p : up.updates) {
                myreplicas.get(p.id).addState(p.state);
            }

            if (receivedLocal == partitions.size() - 1 && !nextlocal) {

                doAggr();
                receivedLocal = 0;
                nextlocal = true;
                if (receivedAggr == partitions.size() - 1) {
                    doLocal();
                    nextlocal = false;
                    receivedAggr = 0;

                }

            }

        } else if (o instanceof SynMessages.PRINT) {
            FileWriter fw = new FileWriter("output" + ID);
            for (int i=0;i<subgraph.id.length;i++) {
                int node = subgraph.id[i];
                if (subgraph.owns[i]==-1) {
                    fw.write(node + " " + subgraph.getState(node) + "\n");
                } else if (subgraph.owns[i] == ID) {
                    fw.write(node + " " + myreplicas.get(node).getState() + "\n");
                }
            }
            fw.close();
            master.tell(new SynMessages.PRINTED(), self());
        } else if (o instanceof SynMessages.SHUTDOWN) {
            System.out.println("TOTAL TIME IN LOCAL = " + (timeinLocal / 1000.0));
            System.out.println("TOTAL TIME IN AGGR = " + (timeinAggr / 1000.0));
            getContext().system().shutdown();
        } else {
            unhandled(o);
        }
    }

}
