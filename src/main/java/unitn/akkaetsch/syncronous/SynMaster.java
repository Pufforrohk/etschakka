package unitn.akkaetsch.syncronous;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.japi.Creator;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import unitn.akkaetsch.syncronous.SynMessages.SHUTDOWN;

public class SynMaster extends UntypedActor {

    int K;
    ArrayList<ActorRef> partitions;
    HashMap<Integer, HashSet<Integer>> finished;

    public static Props props(final int K) {
        return Props.create(new Creator<SynMaster>() {
            @Override
            public SynMaster create() throws Exception {
                return new SynMaster(K);
            }
        });
    }

    public SynMaster(int K) {
        this.K = K;
        partitions = new ArrayList<>();
        for (int i = 0; i < K; i++) {
            partitions.add(null);
        }
        finished = new HashMap<>();
        try {
            log = new FileWriter("masterLog.txt");
            startingTime = System.currentTimeMillis();
            log.write("START " + startingTime + "\n");
        } catch (IOException ex) {
            Logger.getLogger(SynMaster.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    FileWriter log;
    int partitions_checkin = 0;
    int replicas_connected = 0;
    int partitions_printed = 0;
    boolean finishing = false;
    private long startingTime = 0;
    private long checkedinTime = 0;
    private long replicaTime = 0;
    private long finishedTime = 0;
    

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof SynMessages.CHECKIN) {
            SynMessages.CHECKIN message = (SynMessages.CHECKIN) o;
            partitions.set(message.id, getSender());
            partitions_checkin++;
            System.out.println("Partition " + message.id + "has checked in");
            if (partitions_checkin == K) {
                checkedinTime = System.currentTimeMillis();
                log.write("CHECKEDIN " + checkedinTime + "\n");
                log.write("CHECKEDIN_ELAPSED " + (checkedinTime - startingTime) + "\n");
                log.flush();
                for (ActorRef ar : partitions) {
                    ar.tell(new SynMessages.SENDREPLICAS(partitions), self());
                }
            }
        } else if (o instanceof SynMessages.DONE_REPLICAS) {
            replicas_connected++;
            System.out.println("RECEIVED OK FROM " + getSender().path().name());
            if (replicas_connected == K) {
                replicaTime = System.currentTimeMillis();
                log.write("REPLICAS " + replicaTime + "\n");
                log.write("REPLICAS_ELAPSED " + (replicaTime-checkedinTime) + "\n");
                log.flush();
                for (ActorRef ar : partitions) {
                    ar.tell(new SynMessages.START_ETSCH(), self());
                }

            }
        } else if (o instanceof SynMessages.NOTHINGTODO) {
            if (!finishing) {
                SynMessages.NOTHINGTODO mess = (SynMessages.NOTHINGTODO) o;
                if (finished.containsKey(mess.iteration)) {
                    finished.get(mess.iteration).add(mess.pid);
                    if (finished.get(mess.iteration).size() == K) {
                        for (ActorRef ar : partitions) {
                            ar.tell(new SynMessages.PRINT(), self());
                        }
                        System.out.println("FINISHED" + mess.iteration);
                        finishing = true;
                    }
                } else {
                    finished.put(mess.iteration, new HashSet<Integer>());
                    finished.get(mess.iteration).add(mess.pid);
                }
            }
        } else if (o instanceof SynMessages.PRINTED) {
            partitions_printed++;
            if (partitions_printed == K) {
                finishedTime = System.currentTimeMillis();
                log.write("ENDED " + finishedTime + "\n");
                log.write("COMPUTATION_ELAPSED " + (finishedTime-replicaTime) + "\n");
                log.write("TOTAL_EXECUTION " + (finishedTime-startingTime) + "\n");
                log.close();
                for(ActorRef ar:partitions)
                    ar.tell(new SHUTDOWN(), self());
                context().system().shutdown();
            }
        } else {
            unhandled(o);
        }
    }

}
