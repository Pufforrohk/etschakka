package unitn.akkaetsch.syncronous;

import akka.actor.ActorRef;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class SynMessages {

    //Sent from main to master to start initialization
    public static class CHECKIN implements Serializable {

        public final int id;

        public CHECKIN(int i) {
            id = i;
        }
    }

    public static class SENDREPLICAS implements Serializable {

        final ArrayList<ActorRef> partitions;

        public SENDREPLICAS(ArrayList<ActorRef> partitions) {
            this.partitions = partitions;
        }
    }

    public static class MYREPLICAS implements Serializable {

        final ArrayList<Integer> nodes;
        final int partition_id;
        public MYREPLICAS(ArrayList<Integer> nodes, int id) {
            this.nodes = new ArrayList<>(nodes);
            this.partition_id = id;
        }

    }

    public static class ANSWER_REPLICAS implements Serializable {

        final HashMap<Integer, Integer> other_replicas;
        final HashMap<Integer, ArrayList<Integer>> your_replicas;

        public ANSWER_REPLICAS(HashMap<Integer, Integer> or, HashMap<Integer, ArrayList<Integer>> yr) {
            other_replicas = or;
            your_replicas = yr;
        }
    }

    public static class DONE_REPLICAS implements Serializable {
    }

    //Sent from master to partitions to let them know
    public static class START_ETSCH implements Serializable {
    }

    public static class Pair implements Serializable {

        public final int id;
        public final Object state;

        public Pair(int i, Object s) {
            id = i;
            state = s;
        }
    }

    //Updates
    public static class UPDATES_FROM_LOCAL implements Serializable {

        final int partition_id;
        final ArrayList<Pair> updates;

        
        public UPDATES_FROM_LOCAL(int pid, ArrayList<Pair> updates) {
            this.partition_id = pid;
            this.updates = updates;
        }

    }

    //Updates
    public static class UPDATES_FROM_AGGR implements Serializable {

        final int partition_id;
        final ArrayList<Pair> updates;

        public UPDATES_FROM_AGGR(int pid, ArrayList<Pair> updates) {
            this.partition_id = pid;
            this.updates = updates;
        }

    }

    public static class NOTHINGTODO implements Serializable {

        final int pid;
        final int iteration;

        public NOTHINGTODO(int p, int it) {
            pid = p;
            iteration = it;
        }
    }

    public static class PRINT implements Serializable {

    }

    public static class PRINTED implements Serializable {

    }

    public static class SHUTDOWN implements Serializable {

    }
}
