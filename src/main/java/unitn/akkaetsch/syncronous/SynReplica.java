package unitn.akkaetsch.syncronous;

import java.util.ArrayList;
import unitn.akkaetsch.structures.Algorithm;

public class SynReplica {

    final int id;
    public ArrayList<Integer> partitions;
    private Object obj;
    public ArrayList<Object> states;
    public boolean changed;
    private boolean setted = false;

    public void addState(Object state) {
        states.add(state);
    }

    public Object getState() {
        return obj;
    }

    public void setState(Object state) {
        if (setted) {
            System.out.println("Trying to reset replica state");
            new Exception().printStackTrace();
            System.exit(1);
        }
        setted = true;
        if (state == null) {
            System.out.println("You Fukken Kidding Me?");
            new Exception().printStackTrace();
            System.exit(1);

        }
        obj = state;
    }

    public boolean update(Algorithm a) {
        if (obj == null) {
            System.out.println("What's going on?" + setted);
            new Exception().printStackTrace();
            System.exit(1);
        }
        if (states.isEmpty()) {
            changed = false;
            return false;
        }
        Object old = obj;
        obj = a.Aggregation(obj, states);

        changed = !old.equals(obj);
        boolean sendback = false;
        if (states.size() == 1) {
            sendback = true;
        } else {
            for (Object state : states) {
                if (!state.equals(obj)) {
                    sendback = true;
                }
            }
            if(!old.equals(obj))
                sendback = true;
        }
        states.clear();
        return sendback;
    }

    public SynReplica(int id) {
        this.id = id;
        partitions = new ArrayList<>();
        obj = null;
        states = new ArrayList<>();
    }

}
